import React, { useState } from 'react';
import { Layout } from 'antd';
import Home from '../components/pages/homePage'

const Index = (props) => {
  return (
        <div>
            <Home/>
            <style jsx global>
               {`
               @font-face {
                     font-family: Rubik-Black;
                     src: url(fonts/rubik/Rubik-Black.ttf);
                  }
                @font-face {
                    font-family: Rubik-Light;
                    src: url(fonts/rubik/Rubik-Light.ttf);
                }
                @font-face {
                     font-family: Rubik-Regular;
                     src: url(fonts/rubik/Rubik-Regular.ttf);
                }
               `}
            </style>
        </div>
    );
};

export default Index;