import dynamic from 'next/dynamic'
import Head from 'next/head'

const theme = 'light-themes'

const Home = () => {

	const MainTheme = dynamic(() => import(`../themes/${theme}`))

  return (
  	<div>
      <Head>
      <title>Modena - Home</title>
      <meta name="description" content="MODENA dimulai pada tahun 1960-an sebagai salah satu produsen peralatan memasak pertama di Emilia Romagna, Italia 
      – wilayah yang terkenal dengan industrialisme dan desainnya, menjadi rumah bagi merek mobil dan peralatan konsumen ternama.
      Selanjutnya pada tahun 1970-an, MODENA banyak berinvestasi dalam R&D dan desain produk yang mengarah pada peluncuran berbagai kategori produk. 
      Dengan industrialisasi yang bergeser ke timur dan globalisasi yang meningkat, 
      pada 1980-an MODENA mengalami restrukturisasi, dan produksi secara bertahap bergeser ke Asia untuk mengambil keuntungan dari kemampuan manufaktur dan teknik di kawasan itu. 
      Sebuah merek terpadu, Nama MODENA, diambil oleh Tomas Jizhar dari nama kota industri yang indah di mana semuanya dimulai.
      Saat ini, didukung oleh studio desain inovatif di Italia dan pusat R&D canggih serta fasilitas manufaktur di seluruh dunia, 
      MODENA berupaya menjadi pendorong menuju dunia yang saling terhubung untuk membentuk masa depan. Setiap hari, 
      MODENA bergerak dengan misi membuat hidup lebih mudah dengan berbagai macam produknya mulai dari solusi rumah, 
      profesional hingga energi dan mobilitas." />
      <link rel="icon" href="logo/modena-logo-white.svg" />
      </Head>
        <MainTheme/>
    </div>
  )
}

export default Home