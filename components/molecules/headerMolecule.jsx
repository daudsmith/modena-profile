import React, { useState } from 'react';
import { Layout } from 'antd';

const HeaderOrganism = (props) => {
  return (
    <div>
        <span className='content-title' level={2}>Smartest Solutions For Home And Living</span>
        <img className='first-image' src="images/first-image.png"/>
    </div>
    );
};

export default HeaderOrganism;