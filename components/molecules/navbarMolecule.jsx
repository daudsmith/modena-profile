import React, { useState } from 'react';
import { Layout } from 'antd';
import { AiOutlineShoppingCart, AiOutlineUser } from "react-icons/ai";

const NavbarMolecules = (props) => {
    const [isNavExpanded, setIsNavExpanded] = useState(false);

  return (
        <nav className="navigation">
            <a href="/">
                <img className="brand-name" src="logo/modena-logo-white.svg" />
            </a>
                <div
                    className={
                        isNavExpanded ? "navigation-menu expanded" : "navigation-menu"
                    }
                >
                    <ul>
                        <li>
                            <a className="navlink-style" href="#">Products</a>
                        </li>
                        <li>
                            <a className="navlink-style" href="#">Culinaria</a>
                        </li>
                        <li>
                            <a className="navlink-style" href="#">Kitchen Design</a>
                        </li>
                        <li>
                            <a className="navlink-style" href="#">Blog</a>
                        </li>
                        <li>
                            <a className="navlink-style" href="#">Promotions</a>
                        </li>
                        <li>
                            <a className="navlink-style" href="#">Services</a>
                        </li>
                        <li>
                            <a className="navlink-style" href="#">Our Location</a>
                        </li>
                    </ul>
                </div>
                <a className="right-id-style" href="#">ID |
                    <span className='right-eng-style'> EN</span>
                </a>
                <a className="icon-style" href="#">
                        <AiOutlineUser size={22}/>
                </a>
                <a className="icon-style" href="#">
                        <AiOutlineShoppingCart size={22}/>
                </a>
                <button
                    className="hamburger"
                    onClick={() => {
                    setIsNavExpanded(!isNavExpanded);
                    }}
                >
                    <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="white"
                    >
                    <path
                        fillRule="evenodd"
                        d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z"
                        clipRule="evenodd"
                    />
                    </svg>
                </button>
        </nav>
    );
};

export default NavbarMolecules;