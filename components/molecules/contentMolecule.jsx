import React, { useState } from 'react';
import { Layout, Col, Row, Button, Card, Form, Input } from 'antd';
import { AiOutlineArrowRight } from "react-icons/ai";

const { Content } = Layout;

const ContentOrganism = (props) => {

  return (
    <div>
      <Content className="content-style">
        <span className='title-content-1'>Step Into Our Virtual Showroom</span>
      </Content>
      <Content className="row-content">
        <Row>
            <Col Col span={12}>
              <span className='content-title' >
                Suryo
                <p className='content-subtitle'>
                  Jln. Suryo No. 22, Jakarta 12180 Tel : (62-21) 722.7813
                </p>
                <div className="site-button-ghost-wrapper">
                  <Button ghost className='btn-primary'>View Showroom <AiOutlineArrowRight/></Button>
                </div>
              </span>
              <img className='first-image' src="images/suryo.svg"/>
            </Col>
              <Col span={12}>  
              <span className='content-title'>
                Satrio
                <p className='content-subtitle'>
                Jln. Prof. Dr. Satrio C4 No. 13, Jakarta 12950 Tel : (62-21) 2996.9588
                </p>
                <div className="site-button-ghost-wrapper">
                  <Button className='btn-primary' ghost>View Showroom <AiOutlineArrowRight/></Button>
                </div>
              </span>
              <img className='first-image' src="images/satrio.svg"/>
            </Col>
        </Row>
      </Content>
      <Content className="content-style">
        <span className='title-content-2'>End-to-end Home Solution - And Beyond</span>
      </Content>
      <Content className="row-content-container">
        <Row>
            <Col Col span={12}>
              <span className='content-product' >
                Cleaning
                <p className='content-subproduct'>
                  Redefine your household Hygiene
                </p>
              <div className="site-button-ghost-product">
                  <Button ghost className='btn-secondary'>Browse Product <AiOutlineArrowRight/></Button>
              </div>
              </span>
              <img className='first-image' src="images/image-1.svg"/>
            </Col>
              <Col span={12}>  
              <span className='content-product' >
                Cooking
                <p className='content-subproduct'>
                  Redefine your household Hygiene
                </p>
                <div className="site-button-ghost-product">
                  <Button ghost className='btn-secondary'>Browse Product <AiOutlineArrowRight/></Button>
              </div>
              </span>
              <img className='first-image' src="images/image-2.svg"/>
            </Col>
        </Row>
        <Row>
            <Col Col span={12}>
              <span className='content-product' >
                Cooling
                <p className='content-subproduct'>
                  Redefine your household Hygiene
                </p>
                <div className="site-button-ghost-product">
                  <Button ghost className='btn-secondary'>Browse Product <AiOutlineArrowRight/></Button>
              </div>
              </span>
              <img className='first-image' src="images/image-3.svg"/>
            </Col>
              <Col span={12}>  
              <span className='content-product' >
                Profesional
                <p className='content-subproduct'>
                  Redefine your household Hygiene
                </p>
                <div className="site-button-ghost-product">
                  <Button ghost className='btn-secondary'>Browse Product <AiOutlineArrowRight/></Button>
              </div>
              </span>
              <img className='first-image' src="images/image-4.svg"/>
            </Col>
        </Row>
      </Content>
    </div>
    );
};

export default ContentOrganism;