import React, { useState } from 'react';
import { Layout, Col, Row, Button, Card, Form, Input } from 'antd';
import { AiFillFacebook, AiFillYoutube, AiFillInstagram } from "react-icons/ai";

const { Content } = Layout;

const { TextArea } = Input;

const FooterOrganism = (props) => {

  return (
    <div>
        <Content className='row-footer'>
          <Row>
            <Col span={8}>
              <img className='brand-footer' src="logo/footer-logo.svg"/>
            </Col>
            <Col span={8}>
              <span className='footer-content'>Contact Us</span>
            </Col>
            <Col span={8}>
              <span className='footer-content'>Menu</span>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <span className='about-content'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
              Integer ac lectus a leo consequat convallis. Lorem ipsum dolor sit amet, 
              consectetur adipiscing elit. Suspendisse accumsan ut magna eu aliquam. 
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pellentesque felis congue risus laoreet, 
              sed aliquet elit faucibus. Phasellus ipsum velit, viverra eget justo at, 
              feugiat accumsan justo. Nullam nec velit convallis, posuere arcu scelerisque, facilisis felis. 
              Morbi eget neque sit amet arcu fringilla venenatis ac vel urna. Vivamus eu lorem vitae nulla efficitur luctus. 
              Nam ante est, varius non gravida ut, iaculis commodo metus. Integer eu interdum erat, efficitur vulputate lorem. 
              Cras sed justo vitae sapien rutrum gravida et cursus libero. Pellentesque et tincidunt tortor.
              </span>
            </Col>
            <Col span={8}>
              <Form className="form-footer" >
                <Form.Item name="nama">
                  <Input placeholder="Nama" />
                </Form.Item>
                <Form.Item name="email">
                  <Input placeholder="Email"/>
                </Form.Item>
                <Form.Item>
                  <TextArea rows={4} />
                </Form.Item>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form>
            </Col>
            <Col span={8}>
              <div className="menu-footer">
                <ul>
                  <li>
                    Home
                  </li>
                  <li>
                    About
                  </li>
                  <li>
                    Catalog
                  </li>
                  <li>
                    Contact Us
                  </li>
                </ul>
              </div>
              <Row className='footer-social-media'>
                <Col span={8}>
                  <a><AiFillFacebook size={22}/></a>
                </Col>
                <Col span={8}>
                <a><AiFillYoutube size={22}/></a>
                </Col>
                <Col span={8}>
                <a><AiFillInstagram size={22}/></a>
                </Col>
              </Row>
              <span className='copy-content'>
                copyright 2021 © MODENADevTest
              </span>
            </Col>
          </Row>
        </Content>
    </div>
    );
};

export default FooterOrganism;