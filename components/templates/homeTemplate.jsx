import React, { useState } from 'react';
import { Layout } from 'antd';
import HeaderOrganism from '../organisms/headerOrganism'
import ContentOrganism from '../organisms/contentOrganism'
import FooterOrganism from '../organisms/footerOrganism'

const HomePage = (props) => {
  return (
    <div className="theme-layout">
        <HeaderOrganism/>
        <ContentOrganism/>
        <FooterOrganism/>
        <style jsx global>
                {`
                    .theme-layout {
                        background-color: #FFFFFF;
                        width: 100%;
                        height: 100%;
                    }
                    .content-footer {
                        width: 100%;
                        height: 100%
                    }
                    .form-footer {
                        margin-left: 40%;
                        margin-top: 10%;
                    }
                    .menu-footer {
                        margin-left: 39%;
                        margin-top: 9%;
                        position: absolute;
                        font-family: Rubik-Regular;
                        font-size: 0.8vw;
                        color: #95a5a6;
                    }
                    .footer-social-media {
                        margin-left: 42%;
                        margin-top: 40%;
                        width: 35%;
                    }
                    .site-button-ghost-wrapper {
                        margin-top: 30%;
                        size: 16px;
                    }
                    .btn-primary {
                        display: inline-block;
                    }
                    .site-button-ghost-product {
                        margin-top: 18%;
                        margin-left: 65%;
                        size: 20px;
                    }
                    .btn-secondary {
                        display: inline-block;
                    }
                    .logo-image {
                        align-items: center;
                        width: 150px;
                        height: 55.83px;
                        left: 61px;
                        top: 46px;
                    }
                    .first-image {
                        filter: brightness(60%);
                        width: 100%;
                        max-width: 1920px;

                    }
                    .content-text {
                        font-family: Rubik-Light;
                        font-size: 14px;
                        text-align: center;
                        margin: auto;
                        padding: auto;
                    }
                    .content-title {
                        position: absolute;
                        font-family: Rubik-Regular;
                        color: #FFFFFF;
                        font-size: 4vw;
                        font-weight: 500;
                        text-align: center;
                        width: 40%;
                        margin-top: 21%;
                        margin-left: 31%;
                        z-index: 2;
                    }
                    .content-subtitle {
                        position: absolute;
                        font-family: Rubik-Regular;
                        color: #FFFFFF;
                        font-size: 1vw;
                        font-weight: 400;
                        text-align: center;
                        justify-content: center;
                        margin-left: 10%;
                        width: 80%;
                    }
                    .content-product {
                        position: absolute;
                        font-family: Rubik-Regular;
                        color: #FFFFFF;
                        font-size: 3vw;
                        font-weight: 500;
                        width: 100%;
                        margin-left: 5%;
                        margin-top: 5%;
                        z-index: 2;
                    }
                    .content-subproduct {
                        position: absolute;
                        font-family: Rubik-Regular;
                        color: #FFFFFF;
                        font-size: 1vw;
                        font-weight: 400;
                        margin-left: 1%;
                        width: 100%;
                    }
                    .navlink-style {
                        font-family: Rubik-Light;
                        color: #FFFFFF;
                        font-weight: 400;
                        font-size: 14px;
                    }
                    .right-id-style {
                        align-items: center;
                        font-family: Rubik-Light;
                        color: #FFFFFF;
                        font-weight: Bold;
                        font-size: 14px;
                        margin-left: 21%;
                        margin-top: 4.2%;
                    }
                    .right-eng-style {
                        font-family: Rubik-Light;
                        color: #f1c40f;
                        font-weight: Bold;
                        font-size: 14px;
                    }
                    .icon-style {
                        color: #FFFFFF;
                        align-items: center;
                        margin-left: 1.5%;
                        margin-top: 4%;
                    }
                    .navigation {
                        position: absolute;
                        display: flex;
                        align-items: right;
                        height: 90px;
                        width: 100%;
                        padding: 0.5rem 0rem;
                        background-color: rgba(0, 0, 0, 0);
                        z-index: 1;
                    }

                    .brand-name {
                        text-decoration: none;
                        align-items: center;
                        margin-left: 42%;
                        margin-top: 29%;
                    }
                    .brand-footer {
                        text-decoration: none;
                        align-items: center;
                        margin-left: 10%;
                        margin-top: 2%;
                    }

                    .navigation-menu {
                        margin-left: 9%;
                        margin-top: 4%;
                    }

                    .navigation-menu-right {
                        margin-left: 4%;
                        margin-top: 4%;
                    }

                    .navigation-menu ul {
                        display: flex;
                        padding: 0;
                    }

                    .navigation-menu li {
                        list-style-type: none;
                        margin: 0 1rem;
                    }

                    .navigation-menu li a {
                        text-decoration: none;
                        display: block;
                        width: 100%;
                    }

                    .hamburger {
                        border: 0;
                        height: 23px;
                        width: 23px;
                        padding: 0.3rem;
                        border-radius: 50%;
                        background-color: #0000006e;
                        cursor: pointer;
                        transition: background-color 0.2s ease-in-out;
                        position: absolute;
                        top: 32%;
                        right: 15px;
                        transform: translateY(-50%);
                        display: none;
                    }

                    .hamburger:hover {
                        background-color: #2642af;
                    }
                    .content-style {
                        width: 100%;
                        height: 20%
                    }
                    .title-content-1 {
                        position: absolute;
                        font-family: Rubik-Regular;
                        font-size: 1.9vw;
                        font-weight: bold;
                        text-align: center;
                        margin-top: 3%;
                        margin-left: 36%;
                    }
                    .title-content-2 {
                        position: absolute;
                        font-family: Rubik-Regular;
                        font-size: 1.9vw;
                        font-weight: bold;
                        text-align: center;
                        margin-top: 11%;
                        margin-left: 32%;
                    }
                    .footer-content {
                        position: absolute;
                        font-family: Rubik-Regular;
                        font-size: 1.1vw;
                        font-weight: bold;
                        text-align: left;
                        margin-top: 1%;
                        margin-left: 42%;
                    }
                    .about-content {
                        position: absolute;
                        font-family: Rubik-Regular;
                        font-size: 0.7vw;
                        color: #95a5a6;
                        text-align: left;
                        margin-top: 10%;
                        margin-left: 10%;
                    }
                    .copy-content {
                        position: absolute;
                        font-family: Rubik-Regular;
                        font-size: 0.8vw;
                        color: #95a5a6;
                        text-align: center;
                        margin-top: 10%;
                        margin-left: 42%;
                    }
                    .row-content {
                        margin-top: 9%;
                    }

                    .row-content-container {
                        margin-top: 9%;
                        padding: 8rem
                    }
                    
                    .row-footer {
                        margin-bottom: 3%;
                    }
                    

                    @media screen and (max-width: 768px) {

                    .hamburger {
                        display: block;
                    }
                    .row-footer {
                        width: 100%;
                        height: 40%;
                        position: absolute;
                    }
                    .btn-primary {
                        width: 30px;
                        height: 10px;
                        font-size: 3px;
                        margin: 0 auto;
                    }
                    .btn-secondary {
                        width: 25px;
                        height: 8px;
                        font-size: 3px;
                        margin: 0 auto;
                    }
                    .right-id-style {
                        align-items: center;
                        font-family: Rubik-Light;
                        color: #FFFFFF;
                        font-weight: Bold;
                        font-size: 7px;
                        margin-left: 22%;
                        margin-top: 6%;
                    }

                    .right-eng-style {
                        font-family: Rubik-Light;
                        color: #f1c40f;
                        font-weight: Bold;
                        font-size: 7px;
                    }
                    
                    .form-footer {
                        margin-left: 40%;
                        margin-top: 10%;
                        width: 80%;
                        height: 30%;
                    }
                    .icon-style {
                        color: #FFFFFF;
                        align-items: center;
                        margin-left: 1.5%;
                        margin-top: 2.4%;
                    }

                    .row-content-container {
                        margin-top: 9%;
                        padding: 2rem
                    }
                    
                    .brand-footer {
                        text-decoration: none;
                        align-items: center;
                        margin-left: -1%;
                        margin-top: 2%;
                        width: 100%;
                        height: 50%;
                    }


                    .brand-name {
                        text-decoration: none;
                        align-items: center;
                        margin-left: 14%;
                        margin-top: 5%;
                        width: 100%;
                        height: 50%;
                    }

                    .navigation-menu ul {
                        flex-direction: column;
                        position: absolute;
                        top: 60px;
                        left: 0;
                        width: 100%;
                        height: calc(100vh - 60px);
                        background-color: white;
                        border-top: 1px solid black;
                        display: none;
                    }

                    .navigation-menu li {
                        text-align: center;
                        margin: 0;
                    }

                    .navigation-menu li a {
                        color: black;
                        width: 100%;
                        padding: 1.5rem 0;
                    }

                    .navigation-menu li:hover {
                        background-color: #eee;
                    }

                    .navigation-menu.expanded ul {
                        display: block;
                    }
                    }
                `}
            </style>
    </div>
    );
};

export default HomePage;