import React, { useState } from 'react';
import FooterMolecule from '../molecules/footerMolecule'
import { Layout } from 'antd';

const { Content } = Layout;

const FooterOrganism = (props) => {

  return (
    <Layout>
      <Content className="content-footer">
        <FooterMolecule/>
      </Content>
    </Layout>
    );
};

export default FooterOrganism;