import React, { useState } from 'react';
import { Layout } from 'antd';
import NavbarOrganism from '../organisms/navbarOrganism'
import HeaderMolecule from '../molecules/headerMolecule'

const { Content } = Layout;

const HeaderOrganism = (props) => {
  return (
    <Layout>
      <Content>
            <NavbarOrganism/>
            <HeaderMolecule/>
      </Content>
    </Layout>
    );
};

export default HeaderOrganism;